'use-strict';

const utils = require('../src/utils.js');


describe('Ensure correctness of utility functions', () => {
  test('Test isInt', () => {
    expect(utils.isInt(5)).toBe(true);
    expect(utils.isInt(-1)).toBe(true);
    expect(utils.isInt(5.5)).toBe(false);
    expect(utils.isInt('5')).toBe(false);
  });
  test('Test isWhole', () => {
    expect(utils.isWhole(5)).toBe(true);
    expect(utils.isWhole(-1)).toBe(false);
    expect(utils.isWhole(5.5)).toBe(false);
    expect(utils.isWhole('5')).toBe(false);
  });
  test('Test inArray', () => {
    expect(utils.inArray([1, 2, 3], 2)).toBe(true);
    expect(utils.inArray([1, 2, 3], 4)).toBe(false);
    expect(utils.inArray([], undefined)).toBe(false);
  });
  test('Test eachInArray', () => {
    expect(utils.eachInArray([1, 2, 3], [1, 1, 3])).toBe(true);
    expect(utils.eachInArray([1, 2, 3], [])).toBe(true);
    expect(utils.eachInArray([], [1, 2])).toBe(false);
  });
  test('Test for value in object', () => {
    const testObj = {test1: 5, test2: 'hello'};
    expect(utils.hasValue(testObj, 'hello')).toBe(true);
    expect(utils.hasValue(testObj, 6)).toBe(false);
  });
});
