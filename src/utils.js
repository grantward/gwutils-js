'use-strict';

//Return if number is integer
function isInt(num) {
  if (typeof num === 'number') {
    return (0 === (num % 1));
  }
  return false;
}

//Return if number is whole (positive integer)
function isWhole(num) {
  if (isInt(num)) {
    return (0 <= num);
  }
  return false;
}

//Return if an object is an array
function isArray(obj) {
  return (Object.prototype.toString.call(obj) === '[object Array]');
}

//Return if obj is in array
function inArray(array, obj) {
  for (const idx in array) {
    if (obj == array[idx]) {
      return true;
    }
  }
  return false;
}

//Return if each component of array is in array
function eachInArray(checkAgainst, checked) {
  for (const idx in checked) {
    if (!inArray(checkAgainst, checked[idx])) {
      return false;
    }
  }
  return true;
}

//Returns true if obj has a key with value of value, false otherwise
function hasValue(obj, value) {
  for (key in obj) {
    if (obj[key] === value) return true;
  }
  return false;
}

module.exports = {
  isInt,
  isWhole,
  isArray,
  inArray,
  eachInArray,
  hasValue,
};
